# Fract-ol

This repository contains the __Fract'ol__ project of 42

__Start:__  
	
	make
	./fractol [Julia], [Mandelbrot], [Tricorn]   

__Movement:__  

	Press UP key    :	Move up
	Press DOWN key  :	Move down
	Press LEFT key  :	Move to the left
	Press RIGHT key :	Move to the right
	Scroll          :	Zoom in/Zoom out
__Options:__  

	Press r key	:	+ Red
 	Press g key	:	+ Green
 	Press b key	:	+ Blue

	Press e key	:	Red -
 	Press f key	:	Green -
	Press v key	:	Blue -

Enjoy it!  

![mandelbrot.png](https://raw.githubusercontent.com/dvolberg/Fract-ol/master/mandelbrot.png)
![tricorn.png](https://raw.githubusercontent.com/dvolberg/Fract-ol/master/tricorn.png)
![julia.png](https://raw.githubusercontent.com/dvolberg/Fract-ol/master/julia.png)